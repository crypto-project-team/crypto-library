cmake_minimum_required( VERSION 3.8...3.15)
    list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/cmake")
    project( Myproject VERSION 1.0
            DESCRIPTION "Hello world"
            LANGUAGES CXX)
enable_testing()

set(CMAKE_CXX_STANDART 14)

add_subdirectory("${PROJECT_SOURCE_DIR}/extern/tfhe/src" "extern/tfhe/src")

add_library( crypto_int STATIC crypto_int.cpp)
add_executable( cloud cloud.cpp)
add_executable( client client.cpp)
include_directories(crypto_int PUBLIC extern/tfhe/src/include)
target_link_libraries(crypto_int "/home/dmitry/Workspace/logic-server/src/crypto_library/bld/extern/tfhe/src/libtfhe/libtfhe-nayuki-portable.so")
target_link_libraries( cloud PUBLIC crypto_int)
target_link_libraries( client PUBLIC crypto_int)

