## Building

1) create folder extern
2) in extern clone tfhe lib ( https://github.com/tfhe/tfhe , https://github.com/tfhe/tfhe.git)
3) in CMakeLists.txt comment strings:

    `add_library( crypto_int STATIC crypto_int.cpp)`

   ` add_executable( client client.cpp)    `

    `add_executable( cloud cloud.cpp)    `

    `include_directories(crypto_int PUBLIC extern/tfhe/src/include)`
    
    `target_link_libraries(crypto_int "crypto-lib/build/extern/tfhe/src/libtfhe/libtfhe-nayuki-portable.so")`

    `target_link_libraries( client PUBLIC crypto_int)`

    `target_link_libraries( cloud PUBLIC crypto_int)
    `

4) in root create dir bld, in it 
`cmake ..; make`
5) it should compile without errors, find `libtfhe-nayuki-portable.so`
6) in CMakeLists.txt uncomment lines, change
`crypto-lib/build/extern/tfhe/src/libtfhe/libtfhe-nayuki-portable.so`
to your full path
7) in NEW folder bld2... do:

    `cmake ..`

    `make client` - to make client
    
    or

    `make server` - to make server

8) in case of `error: unable to find numeric literal operator ‘operator""i’`,
add `using namespace std::complex_literals;` to the header of `tfhe/src/libtfhe/fft_processors/nayuki/fft_processor_nayuki.cpp`
9) create folders: `data_to_encrypt`, `encrypted_data` for client

    `cloud_input`, `cloud_output` - for server 
    
10) Move python scripts to the same folder as the executables for client and server.

11) done

## Running

`python test.py` - for client

`python test_cloud.py` - for server
