#include "crypto_int.h"

#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <dirent.h>
#include <cstring>
#include <exception>
#include <stdexcept>

using std::string;
using std::cerr;
using std::endl;
using std::cout;
using std::stringstream;
using std::exception;
using std::invalid_argument;
using std::logic_error;


int main(int argc, char **argv) {
    if (argc < 4) {
        throw invalid_argument("Misusing: specify job, key (and tasks - for encrypt)");
    }
    string task = string(argv[1]);
    if (task != "--encrypt" && task != "--decrypt") {
        throw invalid_argument("Misusing: invalid task argument");
    }
    int numeric_key = atoi(argv[2]);

    if (!numeric_key) {
        throw invalid_argument("Misusing: invalid encryption key value. Key should be a number.");
    }
    string path = string(argv[3]);

    Key key(numeric_key);

    if (task == "--encrypt") {
        for (int i = 4; i < argc; i++) {
            char *end;
            char *p = argv[i];
            for (int f = strtol(p, &end, 10); p != end; f = strtol(p, &end, 10)) {
                if (errno == ERANGE) {
                    throw invalid_argument("Failed to convert input value to int: ");
                }
                crypto_int value(f, key);
                stringstream iss;
                iss << i-3;
                string p_path = iss.str();
                string enctypted_data_path = path + "/encrypted_data";
                FILE *output = fopen((enctypted_data_path + "/" + p_path).c_str(), "w");
                if (!output) {
                    throw logic_error("Opening error! " + enctypted_data_path + "/" + p_path);
                }
                value.save(output);
                cout << enctypted_data_path + "/" + p_path << " ";
                fclose(output);
                p = end;
            }
        }
        cout << endl;
    } else if (task == "--decrypt") {
        FILE *input = fopen(path.c_str(), "r");
        if(!input){
            throw logic_error("Opening error! " + path);
        }
        crypto_int encrypted_value;
        encrypted_value.restore(input);
        fclose(input);
        int value = encrypted_value.decrypt(key);
        cout << value;
    }
}
