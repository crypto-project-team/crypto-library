import os

from typing import List


class HomoEncryptor:
    def __init__(self, key: int):
        self.key = str(key)

    def encrypt(self, nums: List[str], directory_path: str, exec_path: str):
        encrypted_data_dir = f'{directory_path}/encrypted_data/'
        if not os.path.exists(encrypted_data_dir):
            os.mkdir(encrypted_data_dir)
        output = os.popen(f'{exec_path} --encrypt {self.key} {directory_path} {" ".join(nums)}')
        output = output.read()
        return output.split(' ')[:-1]

    def decrypt(self, directory_path: str, exec_path: str):
        output = os.popen(f'{exec_path} --decrypt {self.key} {directory_path}')
        output = output.read()
        return output


class ExpressionProcessor:
    def __init__(self, key):
        self.key = str(key)
        self.encryptor = HomoEncryptor(key)

    def save_expression(self, nums: List[str], ops: List[str], write_path: str, exec_path: str) -> None:
        self.encryptor.encrypt(nums, write_path, exec_path)
        ops_file = f'{write_path}/encrypted_data/ops'
        with open(ops_file, 'w') as f:
            for item in ops:
                f.write("%s" % item)
        f.close()

    def decrypt_result(self, path: str, exec_path: str):
        return self.encryptor.decrypt(path, exec_path)

    # @staticmethod
    # def _debug_move():
    #     src_files = os.listdir('encrypted_data')  # should be done by cloud, now we only copy
    #     for file_name in src_files:
    #         full_file_name = os.path.join('encrypted_data', file_name)
    #         if os.path.isfile(full_file_name):
    #             if file_name != "ops":
    #                 shutil.copy(full_file_name, 'data_to_decrypt')
    #
    # @staticmethod
    # def _debug_clean():
    #     try:
    #         shutil.rmtree('encrypted_data')
    #         shutil.rmtree('data_to_decrypt')  # possibly should be removed in production
    #     except OSError:
    #         pass
    #
    #     try:
    #         os.mkdir('encrypted_data')
    #     except OSError:
    #         pass
    #
    #     try:
    #         os.mkdir('data_to_decrypt')
    #     except OSError:
    #         pass
