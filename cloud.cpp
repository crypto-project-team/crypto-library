#include "crypto_int.h"

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cstring>
#include <exception>
#include <stdexcept>
#include <vector>

using std::string;
using std::cerr;
using std::cout;
using std::endl;
using std::exception;
using std::invalid_argument;
using std::logic_error;
using std::vector;

/*
Params:
1 param - num of numbers
2 param - directory

Example:
2 /home 1 + 1 + 1  
*/
int main(int argc, char **argv) {
    if (argc < 5) {
        throw logic_error("Too small count of params");
    }
    int num_of_nums = atoi(argv[1]);
    cout << "num of nums " << num_of_nums << endl;
    string main_path = string(argv[2]);
    cout << "main path" << main_path << endl;

    cout << "test 1" << endl;

    string path = main_path + "/" + string(argv[3]);
    FILE* input = fopen(path.c_str(), "r");
    if (!input) {
        throw logic_error("bad file?");
    }
    crypto_int answer;
    answer.restore(input);
    fclose(input);

    cout << "test 2" << endl;
    for (uint i = 4; i < num_of_nums * 2 - 2; i = i + 2) {
        cout << i << endl;
        string sign = string(argv[i]);
        string path = main_path + "/" + string(argv[i + 1]);
        FILE* input = fopen(path.c_str(), "r");
        if (!input) {
            throw logic_error("bad file?");
        }   
        crypto_int blyaat;
        blyaat.restore(input);
        fclose(input);
        cout << sign << ":" << path << endl;
        if (sign == "+") {
            answer += blyaat;
        }
        else if (sign == "-") {
            answer -= blyaat;
        }
        else if (sign == "@") {
            answer *= blyaat;
        }
        else {
            throw invalid_argument("Bad operator");
        }
    }
    cout << "test 3" << endl;
    string c_path = "answer";
    FILE *output = fopen((main_path + "/" + c_path).c_str(), "w");
    if (!output) {
        throw logic_error("Opening error!");
    }
    answer.save(output);
    cout << "test 5" << endl;
    fclose(output);
    cout << main_path + "/" + c_path << endl;
    return 0;
}
