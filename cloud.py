import os
from typing import List


def process_expression(dir_path: str, exec_path: str):
    src_files: List[str] = os.listdir(dir_path)
    with open(f"{dir_path}/ops") as f:
        ops: List[str] = list(f.read())
    src_files.remove("ops")
    args: List[str] = [src_files[0]]
    for i in range(0, len(src_files) - 1):
        args.append(ops[i])
        args.append(src_files[i + 1])
    print(args)
    output = os.popen(f'{exec_path} {len(src_files)} {dir_path} {" ".join(args)}')
    output = output.read()
    print(output)
