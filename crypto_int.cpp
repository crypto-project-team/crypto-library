#include "crypto_int.h"

// create + encrypt int to this
crypto_int::crypto_int(int value, const Key &key) {
    crypted = new_TLweSample(params1024_1);
    //std::cout<<"create "<< value<<"\n";
    auto tmp = modSwitchToTorus32(value, M);
    tLweSymEncryptT(crypted, tmp, alpha, key.getKey());
}

// only create ,  full zeroes
crypto_int::crypto_int() {
    crypted = new_TLweSample(params1024_1);
}

crypto_int::~crypto_int() {
    delete_TLweSample(crypted);
}

// with key decrypt
int crypto_int::decrypt(const Key &keyN) {
    return modSwitchFromTorus32(tLweSymDecryptT(crypted, keyN.getKey(), M), M);
}

crypto_int crypto_int::copy() {
    crypto_int for_ret;
    tLweCopy(for_ret.crypted, crypted, params1024_1);
    return for_ret;
}

bool crypto_int::save(FILE *output) {
    export_tlweSample_toFile(output, crypted, params1024_1);
}

bool crypto_int::restore(FILE *input) {
    import_tlweSample_fromFile(input, crypted, params1024_1);
}

crypto_int &operator+=(crypto_int &lhs, const crypto_int &rhs) {
    tLweAddTo(lhs.crypted, rhs.crypted, params1024_1);
    return lhs;
}

crypto_int &operator-=(crypto_int &lhs, const crypto_int &rhs) {
    tLweSubTo(lhs.crypted, rhs.crypted, params1024_1);
    return lhs;
}

crypto_int &operator*=(crypto_int &lhs, const crypto_int &rhs) {
    tLweAddMulTo(lhs.crypted, 1, rhs.crypted, params1024_1);
    return lhs;
}

// non random 
Key::Key(int passwd) {
    _key = new_TLweKey(params1024_1);
    const int32_t N = params1024_1->N;
    const int32_t k = params1024_1->k;

    for (int32_t i = 0; i < k; ++i)
        for (int32_t j = 0; j < N; ++j)
            _key->key[i].coefs[j] = (i + j + passwd) % 2; //rand() % 2;
}

Key::~Key() {
    delete_TLweKey(_key);
}

const TLweKey *Key::getKey() const {
    return _key;
}    
