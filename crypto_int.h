#ifndef CRYPTO_INT
#define CRYPTO_INT

#include "tfhe.h"
#include "tfhe_io.h"
#include "polynomials.h"
#include "lwesamples.h"
#include "lweparams.h"
#include <iostream>
#include <cassert>
#define M 1024
static const double alpha = 1. / (1000.);
static const TLweParams *params1024_1 = new_TLweParams(1024, 2, 0.01, 0.07);

class Key {
public:
    TLweKey *_key;

    Key(int passwd);

    ~Key();

    const TLweKey *getKey() const;
};

class crypto_int {
public:
    TLweSample *crypted;

    int decrypt(const Key &key);

    crypto_int(int value, const Key &key);

    crypto_int();

    ~crypto_int();

    crypto_int copy();

    friend crypto_int &operator+=(crypto_int &lhs, const crypto_int &rhs);

    friend crypto_int &operator-=(crypto_int &lhs, const crypto_int &rhs);

    friend crypto_int &operator*=(crypto_int &lhs, const crypto_int &rhs);

    friend crypto_int &multy_add(const int &d, const crypto_int &lhs);

    bool save(FILE *output);

    bool restore(FILE *input);
};


#endif
